### Running the project:
Use the following steps to run the project

1. Programs needed - Android Studio 2017 or higher
2. Download or clone the entire project to desired folder
3. Launch Android Studio.
4. File->Open->Select downloaded file->OK
5. Wait for project to build then hit shift+F10 to run the project.
