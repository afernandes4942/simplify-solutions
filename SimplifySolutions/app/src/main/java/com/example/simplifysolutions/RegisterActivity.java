package com.example.simplifysolutions;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {

    TextView Label;
    EditText Email, RegUser, RegPassword;
    Button GetStarted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Label = findViewById(R.id.etRegisterLabel);
        Email = findViewById(R.id.etEmail);
        RegUser = findViewById(R.id.etRegName);
        RegPassword = findViewById(R.id.etRegPassword);
        GetStarted = findViewById(R.id.btnRegister);

    }
}
